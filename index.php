<?php
include "library/database.php";
?>

<!doctype html>
<html>
<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<link rel="apple-touch-icon" href="images/apple-touch-icon.png" />
<link rel="apple-touch-startup-image" href="images/ipad-landscape-retina.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape) and (-webkit-min-device-pixel-ratio: 2)" />
<link rel="stylesheet" type="text/css" href="css/uploadifive.css">

<title>美髮店軟件 v2</title>

<link href="css/mobiscroll.custom-2.4.4.min.css" rel="stylesheet" type="text/css" />
<link rel='stylesheet' type='text/css' href='css/fullcalendar.css' />
<link rel='stylesheet' type='text/css' href='css/fullcalendar.print.css' media='print' />
<link rel="stylesheet" href="http://code.jquery.com/mobile/1.3.0/jquery.mobile-1.3.0.min.css" />

<script src="http://code.jquery.com/jquery-1.8.2.min.js"></script>
<script type='text/javascript' src='js/jquery-ui-1.8.23.custom.min.js'></script>
<script type='text/javascript' src='js/fullcalendar.min.js'></script>


<!--Includes-->

<script src="http://code.jquery.com/mobile/1.3.0/jquery.mobile-1.3.0.min.js"></script> 
<script src="js/mobiscroll.custom-2.4.4.min.js" type="text/javascript"></script>
<script src="js/jquery.uploadifive.min.js" type="text/javascript"></script>

<script type='text/javascript'>
	$(document).bind("mobileinit", function(){
		$.mobile.ajaxLinksEnabled(false);				
	});
	$(document).ready(function() {		
		$(".editLastUpdated").live("change", function() {
			var serviceID = $(this).attr("serviceID");
			var newLastUpdated = $(this).val();
			$.ajax({
					url: "updateServiceRecord.php",
					type: "POST",
					data: "serviceID="+serviceID+"&lastUpdated="+newLastUpdated+"&type=lastUpdated"
				}).done(function(response) {
			});			
		});	
		
		$(".editServiceDesc").live("change", function() {
			var serviceID = $(this).attr("serviceID");
			var serviceDesc = $(this).val();
			$.ajax({
					url: "updateServiceRecord.php",
					type: "POST",
					data: "serviceID="+serviceID+"&type=serviceDesc"+"&serviceDesc="+serviceDesc
				}).done(function(response) {
			});					
		});
		
		$(".editServiceDesc").live("change", function() {
			var serviceID = $(this).attr("serviceID");
			var serviceCost = $(this).val();
			$.ajax({
					url: "updateServiceRecord.php",
					type: "POST",
					data: "serviceID="+serviceID+"&type=serviceCost"+"&serviceCost="+serviceCost
				}).done(function(response) {
			});					
		});		
	});
	
	function deleteCustomer(customerID) {
		$.ajax({
				url: "delete.php",
				type: "POST",
				data: "customerID="+customerID
			}).done(function(response) {
				var time = new Date().getTime();		
				window.location = "index.php?"+time;
		});
	}
	
	function addServiceHistory(customerID) {
		if ($("#dateTime").val() != "") {
			var dateTime = $("#datetime-2").val();
			var serviceDesc = $("#serviceDesc").val();
			var cost = $("#cost").val();
			$.ajax({
				url: "addServiceHistory.php",
				type: "POST",
				data: "customerID="+customerID+"&dateTime="+dateTime+"&serviceDesc="+serviceDesc+"&cost="+cost,
			}).done(function(response) {		
				window.location = "index.php?customerID="+customerID;
			});			
		}
	}
	
	function deleteService(customerID) {
		var serviceID = $("#serviceIDStore").attr('serviceID');
		$.ajax({
			url: "deleteServiceHistory.php",
			type: "POST",
			data: "customerID="+customerID+"&serviceID="+serviceID,
		}).done(function(response) {		
			window.location = "index.php?customerID="+customerID;
		});	
	}
	
	function returnRecords() {
		var post_data = $("#serviceRecordForm").serialize();
		$.ajax({
			url: "getServiceRecord.php",
			type: "POST",
			data: post_data,
		}).done(function(response) {	
			$("#recordResponse").replaceWith(response);	
			//$("#table-custom-3").table-columntoggle( "refresh" );	// when jquery mobile 1.31 comes out, update and uncomment
			$("#table-custom-3 tr:odd").css("background-color", "rgba(0,0,0,0.04)");
			$('#serviceRecordForm div').removeClass('ui-btn-active');			
		});				
	}
	
	function editCustomerInfo(databaseFieldName, customerID, editFieldName) {
		$.ajax({
			url: "updateCustomerInfo.php",
			type: "POST",
			data: 'customerID='+customerID+"&fieldName="+databaseFieldName+"&fieldValue="+$("#"+editFieldName).val(),
		}).done(function(response) {	
		});				
	}
	
	
	$('#page4').live('pageshow',function(e,data){  	
		var date = new Date();
		var d = date.getDate();
		var m = date.getMonth();
		var y = date.getFullYear();
		
		if ($("#calendar").is(':empty')) {
			$('#calendar').fullCalendar({
				header: {
					left: 'prev,next today',
					center: 'title',
					right: 'month,agendaWeek,agendaDay'
				},
				aspectRatio: 1.75,
				editable: false,
				events: "json-events.php",
				loading: function(bool) {
					if (bool) $('#loading').show();
					else $('#loading').hide();
				}	
			});		
		}
	});
	
	$("#page4").live("click", function() {
		$("#calendar").fullCalendar('refetchEvents');
	});
	
	$('#page1').live('pageshow', function(event){
		$('#datetime-2, .editLastUpdated').mobiscroll().datetime({				
			theme: 'ios',
			display: 'bubble',
			animate: 'pop',
			dateFormat: "yy-mm-dd",			
			mode: 'scroller'
		}); 
		<?php $timestamp = time();?>
		$(function() {
			$('#file_upload').uploadifive({
				'auto'             : false,
				'multi' : false,
				'formData'         : {
									   'timestamp' : '<?php echo $timestamp;?>',
									   'token'     : '<?php echo md5('unique_salt' . $timestamp);?>',
									   'customerID' : $("#customerID").val()
									 },
				'uploadScript'     : 'uploadifive.php',
				'queueID'          : 'queue',	
				'onUploadComplete' : function(file, data) {
					$('#file_upload').uploadifive('clearQueue');
					$("#profilePic").empty().append("<img src='uploads/"+data+"' style='max-width:300px;max-height:225px;'/>");				
				}
			});
		});	
				
	});	
</script>

<style type="text/css">
.contentBody {
	margin: 0 5px 0 5px;
}

.ui-listview-filter .ui-input-search {
margin: 25px 5px;
}

.contentBody label {
	font-size: 18px;
	font-weight:bold;	
}

#editCustomer, #editCustomer td, #editCustomer th {
	border-collapse:collapse;
	border: 1px solid #999;	
	text-align: left;
}

#editCustomer td, #editCustomer th {
	padding: 10px;	
}

#calendar {
width: 97%;
margin: 14px auto;	
}

.fc-event-title{
   text-shadow:none;
}

#queue {
	overflow: auto;
	margin-bottom: 10px;
	padding: 0 3px 3px;
	width: 300px;
	margin-top: 24px;	
}

.uploadifive-button {
	float: left;
	margin-right: 10px;
}
#profilePic {
	height: 200px;
	width:300px;	
}

#file_upload {
	display:block !important;	
}
</style>
</head>

<body>
        <!-- Home --> 
        <div data-role="page" id="page1">      
            <div data-theme="e" data-role="header">           
                <h3>美髮廳軟件</h3>
	            <?php if (isset($_GET['customerID'])) :?>                  
                <a href="index.php" data-role="button" data-mini="true" data-inline="true" data-theme="b">Back</a>
                <?php endif; ?>
            </div>
            <?php if (!isset($_GET['customerID'])) :?>          
            <div id='custLookupForm' class='contentBody'>                
                <ul data-role="listview" data-autodividers="true" data-filter="true" data-inset="true" id='customersList'>
                	<?php $result_set = $db->query("SELECT customerID, englishName, chineseName, phoneNumber FROM auntie_customer_tbl ORDER BY englishName");
					while($row=$db->fetch_array($result_set, MYSQL_ASSOC)): ?>
                    <li><a href="index.php?customerID=<?php echo $row['customerID'];?>"><?php echo $row['englishName'] . " " . $row['chineseName'] . " | " . $row['phoneNumber']; ?></a></li>  
                    <?php endwhile; ?>
                </ul>             
            </div>          
            <?php endif; ?>
            
            <?php if (isset($_GET['customerID'])) :
				// get customer info
				list($englishName, $chineseName, $phoneNumber) = $db->fast("SELECT englishName, chineseName, phoneNumber FROM auntie_customer_tbl WHERE customerID = '{$_GET['customerID']}'");
			?>
            <div class='contentBody'>
            	<br />
				<table width='100%' id='editCustomer'>
                	<tr>
                	  	<th rowspan="3" style='width:310px;'>
                  				<form style='margin-bottom:10px;' enctype="multipart/form-data" method="post">
                                	<div id='profilePic'>
                                    	<?php
										// Get profile pic
										$profilePath = $db->getValue("SELECT profilePicUrl FROM auntie_customer_tbl WHERE customerID = '{$_GET['customerID']}'");
										if ($profilePath != "") {
											echo "<img src='uploads/$profilePath' style='max-width:300px;max-height:225px;' />";	
										}
										?>
                                    </div>
									<div id="queue"></div>
                                    <input id="file_upload" name="file_upload" type="file" style='display:block;' data-role="none">
                                  <a style="font-size: 16px;position: relative; top: 5px;" href="javascript:void(0)" onclick="$('#file_upload').uploadifive('upload')">上传照片</a>
                                  <input type='hidden' id='customerID' value='<?php echo $_GET['customerID'];?>' />
                                </form>
                        </th>
                    	<th>English Name:</th>
                        <td><input type='text' value='<?php echo $englishName;?>' id='editEnglishName' name='editEnglishName'  onchange="editCustomerInfo('englishName', '<?php echo $_GET['customerID'];?>', 'editEnglishName')" /></td>
                    </tr>
                    <tr>
                      <th>Chinese Name:</th>
                        <td><input type='text' value='<?php echo $chineseName;?>' id='editChineseName' name='editChineseName' onchange="editCustomerInfo('chineseName', '<?php echo $_GET['customerID'];?>', 'editChineseName')"  /></td>
                    </tr>
                    <tr>
                      <th>Phone Number:</th>
                        <td><input type='number' value='<?php echo $phoneNumber;?>' id='editPhoneNumber' name='editPhoneNumber' onchange="editCustomerInfo('phoneNumber', '<?php echo $_GET['customerID'];?>', 'editPhoneNumber')" /></td>
                    </tr>                    
                </table> <br/>
                
                <form>
                    <label for="datetime-2">日期:</label>
                    <input name="datetime-2" id="datetime-2" value="<?php echo date("Y-m-d h:i:s A");?>" />                
                    <label for="serviceDesc">服務項目：</label>
                    <textarea cols="40" rows="8" name="serviceDesc" id="serviceDesc"></textarea>
                    <label for='cost'>消費</label>
                    <input type="number" id='cost' name='cost' />
                </form>                
                 <a href="javascript:addServiceHistory('<?php echo $_GET['customerID']; ?>')" data-role="button" data-theme="a" >添加</a>
                 
                <br/><br/>
                
                <table data-role="table" id="table-custom-2" data-mode="columntoggle" class="ui-body-d ui-shadow table-stripe ui-responsive" data-column-btn-theme="b" data-column-btn-text="顯示..." data-column-popup-theme="a">
                		 <thead>
                           <tr class="ui-bar-d">
                             <th data-priority="1">日期</th>
                             <th data-priority="2">服務項目</th>
                             <th data-priority='3'>消費</th>
                             <th>&nbsp;</th>
                           </tr>
                         </thead>                
                         <tbody>
							<?php
								$result_set = $db->query("SELECT lastUpdated, serviceDesc, cost, serviceID FROM auntie_service_tbl WHERE customerID = '{$_GET['customerID']}' ORDER BY lastUpdated DESC");
								while($row=$db->fetch_array($result_set, MYSQL_ASSOC)):
									$lastUpdated = date("Y-m-d h:i:s A", strtotime($row['lastUpdated']));
									echo "<tr>
											<th width='25%' style='vertical-align: middle;'><input value='$lastUpdated' class='editLastUpdated' serviceID='{$row['serviceID']}'/></td>
										  	<td style='vertical-align: middle;'><input type='text' value='{$row['serviceDesc']}' serviceID='{$row['serviceID']}' class='editServiceDesc' /></td>
											<td width='10%' style='vertical-align: middle;'><input type='number' value='{$row['cost']}' serviceID='{$row['serviceID']}' class='editCost' /></td>
											<td width='5%'><a href='#deleteServiceHistory' data-role='button' data-icon='delete' data-iconpos='notext' data-theme='c' data-inline='true' onclick='$(\"#serviceIDStore\").attr(\"serviceID\",\"{$row['serviceID']}\")' data-rel='dialog' data-transition='pop' >Delete</a></td>
										  </tr>";
									$totalCost += $row['cost'];										  
								endwhile;
							?> 
	                            <tr>
                                	<th colspan='2' style='text-align:right;padding-right:5px;'>總消費：</th>
                                    <th colspan='2'><?php echo number_format($totalCost,2);?></th>
                                </tr>
                         </tbody>
                       </table>  <br/> 
                       <a href="#deleteDialog" data-role="button" data-icon="delete" data-shadow="true" data-theme="a" data-rel="dialog" data-transition="pop">Delete</a>
                       <br/>
            </div>
            <?php endif;?>
            <div data-role="footer" data-id="footer1" data-position="fixed"  data-theme="a">
                <div data-role="navbar">
                    <ul>
                        <li><a data-icon="home"  href="index.php?<?php echo time();?>" class="ui-btn-active ui-state-persist">客人資料</a></li>
		                <li><a href="#page4" data-icon="grid">日曆</a></li>                        
                        <li><a href="#page2" data-icon="plus">新客人</a></li>
                        <li><a href="#page3" data-icon="info">營業記錄</a></li>
                    </ul>
                </div><!-- /navbar -->
            </div><!-- /footer -->                 
        </div>
        <div data-role="page" id="page2">      
            <div data-theme="e" data-role="header">             
                <h3>美髮廳軟件</h3>
            </div>
            <div id='addNewCust'>
            	<br/>
                <form class='contentBody' id='addNewCustomer' name='addNewCustomer' action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
                    <label for="newEngFullName">英文全名：</label>
                    <input type="text" name="newEngFullName" id="newEngFullName" value="" />
                    <label for="newChineseFullName">中文全名：</label>
                    <input type="text" name="newChineseFullName" id="newChineseFullName" value="" />
                    <label for="newTel">電話:</label>
                    <input type="tel" name="newTel" id="newTel" value="" /> <br/>
					<input type='submit' value='添加'  data-theme="a" data-icon="plus"/>
                </form>            	
            </div>  
            <div data-role="footer" data-id="footer2" data-position="fixed"  data-theme="a">
                <div data-role="navbar">
                    <ul>
                        <li><a data-icon="home"  href="index.php?<?php echo time();?>">客人資料</a></li>
		                <li><a href="#page4" data-icon="grid">日曆</a></li>                                                
                        <li><a href="#page2" data-icon="plus" class="ui-btn-active ui-state-persist">新客人</a></li>
                        <li><a href="#page3" data-icon="info">營業記錄</a></li>
                    </ul>
                </div><!-- /navbar -->
            </div><!-- /footer -->                    
        </div>     
        
        <div data-role="page" id="page3">      
            <div data-theme="e" data-role="header">             
                <h3>美髮廳軟件</h3>
            </div>
            <div id='serviceRecord' class='contentBody'>
            	<br/>
                <form method="post" action="" id='serviceRecordForm'>
                     <label for="recordSearchStartDate">查詢開始日期</label>
                     <input type="date" data-clear-btn="false" name="recordSearchStartDate" id="recordSearchStartDate" value="<?php echo date("Y-m-01");?>">
                     <label for="recordSearchEndDate">終止日期</label>
                     <input type="date" data-clear-btn="false" name="recordSearchEndDate" id="recordSearchEndDate" value="<?php echo date("Y-m-t");?>">
                     <br/>
                     <button type="button" data-theme="a" data-icon="search" onclick="returnRecords();return false;">查詢</button> <br/>
                </form>   
                
                <table data-role="table" id="table-custom-3" data-mode="columntoggle" class="ui-body-d ui-shadow table-stripe ui-responsive" data-column-btn-theme="b" data-column-btn-text="顯示..." data-column-popup-theme="a">
                 <thead>
                   <tr class="ui-bar-d">
                     <th data-priority="1">日期</th>
                     <th data-priority='2'>姓名</th>
                     <th data-priority='3'>電話</th>                     
                     <th data-priority="4">服務項目</th>
                     <th data-priority='5'>消費</th>
                   </tr>
                 </thead>                
                 <tbody id='recordResponse'>
                    <?php
						$startDate = date("Y-m-01"); 
						$endDate = date("Y-m-t");
                        $result_set = $db->query("SELECT
                                                        auntie_customer_tbl.englishName,
                                                        auntie_customer_tbl.chineseName,
                                                        auntie_customer_tbl.phoneNumber,
                                                        auntie_service_tbl.cost,
                                                        auntie_service_tbl.serviceDesc,
                                                        auntie_service_tbl.lastUpdated
                                                    FROM
                                                auntie_customer_tbl
                                                INNER JOIN auntie_service_tbl ON auntie_service_tbl.customerID = auntie_customer_tbl.customerID 
												WHERE DATE_FORMAT(auntie_service_tbl.lastUpdated, '%Y-%m-%d') BETWEEN '$startDate' AND '$endDate'
                                                 ORDER BY lastUpdated DESC");
                        while($row=$db->fetch_array($result_set, MYSQL_ASSOC)):
							$lastUpdated = date("Y-m-d h:i:s A", strtotime($row['lastUpdated']));						
                            echo "<tr>
                                    <th style='vertical-align: middle;'>$lastUpdated</td>
                                    <th style='vertical-align: middle;'>{$row['englishName']} {$row['chineseName']}</td>									
                                    <th style='vertical-align: middle;'>{$row['phoneNumber']}</td>																		
                                    <td style='vertical-align: middle;'>{$row['serviceDesc']}</td>
                                    <td style='vertical-align: middle;'>{$row['cost']}</td>
                                  </tr>";
                            $totalCost += $row['cost'];										  
                        endwhile;
                    ?> 
                        <tr>
                            <th colspan='4' style='text-align:right;padding-right:5px;'>總消費：</th>
                            <th><?php echo number_format($totalCost,2);?></th>
                        </tr>
                 </tbody>
               </table>  
               <br /> <br />            	
            </div>  
            <div data-role="footer" data-id="footer3" data-position="fixed"  data-theme="a">
                <div data-role="navbar">
                    <ul>
                        <li><a data-icon="home"  href="index.php?<?php echo time();?>">客人資料</a></li>
		                <li><a href="#page4" data-icon="grid">日曆</a></li>                                                
                        <li><a href="#page2" data-icon="plus">新客人</a></li>
                        <li><a href="#page3" data-icon="info" class="ui-btn-active ui-state-persist">營業記錄</a></li>
                    </ul>
                </div><!-- /navbar -->
            </div><!-- /footer -->                    
        </div>    
        
        <div data-role="page" id="page4">      
            <div data-theme="e" data-role="header">             
                <h3>美髮廳軟件</h3>
            </div>
          	<div id='calendar'></div>
            <div data-role="footer" data-id="footer3" data-position="fixed"  data-theme="a">
                <div data-role="navbar">
                    <ul>
                        <li><a data-icon="home"  href="index.php?<?php echo time();?>">客人資料</a></li>
		                <li><a href="#page4" data-icon="grid"  class="ui-btn-active ui-state-persist">日曆</a></li>                                                
                        <li><a href="#page2" data-icon="plus">新客人</a></li>
                        <li><a href="#page3" data-icon="info">營業記錄</a></li>
                    </ul>
                </div><!-- /navbar -->
            </div><!-- /footer -->                      
        </div>                 
        
        <div data-role="page" id="deleteDialog" data-close-btn="none">
          <div data-role="header">
            <h2>刪除客人資料</h2>
          </div>
          <div data-role="content">
                <a href="javascript:deleteCustomer('<?php echo $_GET['customerID'];?>')" data-role="button">確認刪除</a>
   				<a href="#" data-role="button" data-rel="back">取消</a>
          </div>
        </div>    
        
        <div data-role="page" id="deleteServiceHistory" data-close-btn="none">
          <div data-role="header">
            <h2>刪除服務項目</h2>
          </div>
          <div data-role="content">
                <a href="javascript:deleteService('<?php echo $_GET['customerID'];?>')" data-role="button" id='serviceIDStore'>確認刪除</a>
   				<a href="#" data-role="button" data-rel="back">取消</a>
          </div>
        </div>                         
</body>
</html>

<?php
if (isset($_POST['newTel']) && $_POST['newTel'] != "") :
	// Add to database
	$db->query("INSERT INTO auntie_customer_tbl (englishName, chineseName, phoneNumber) VALUES ('{$_POST['newEngFullName']}', '{$_POST['newChineseFullName']}', '{$_POST['newTel']}')");
endif;
?>