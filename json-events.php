<?php
include "library/database.php";
date_default_timezone_set('America/Vancouver');

$result_set = $db->query("SELECT
auntie_service_tbl.serviceID,
auntie_service_tbl.serviceDesc,
auntie_customer_tbl.chineseName,
auntie_customer_tbl.englishName,
auntie_service_tbl.customerID,
auntie_service_tbl.lastUpdated
FROM
auntie_customer_tbl
INNER JOIN auntie_service_tbl ON auntie_customer_tbl.customerID = auntie_service_tbl.customerID");

$counter = 0;
$output = array();


while($row=$db->fetch_array($result_set, MYSQL_ASSOC)):
	$output[$counter]['id'] = $row['serviceID'];

	$bookedName = ($row['chineseName'] != "") ? $row['chineseName'] : $row['englishName'];
	$output[$counter]['title'] = $bookedName . "@" . date("G:i", strtotime($row['lastUpdated']));
	$output[$counter]['start'] = date("Y-m-d", strtotime($row['lastUpdated']));
	$output[$counter]['url'] = "http://www.steppingbridge.org/auntie/index.php?customerID={$row['customerID']}";
	
	$counter++; 
endwhile;

	echo json_encode($output);

?>