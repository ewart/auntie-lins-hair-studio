<?php
/*
UploadiFive
Copyright (c) 2012 Reactive Apps, Ronnie Garcia
*/
include "library/database.php";

$customerID = $_POST['customerID'];

// Set the upload directory
$uploadDir = "/auntie/uploads/";

// Set the allowed file extensions
$fileTypes = array('jpg', 'jpeg', 'gif', 'png'); // Allowed file extensions

$verifyToken = md5('unique_salt' . $_POST['timestamp']);

if (!empty($_FILES) && $_POST['token'] == $verifyToken) {
	$tempFile   = $_FILES['Filedata']['tmp_name'];
	$uploadDir  = $_SERVER['DOCUMENT_ROOT'] . $uploadDir;
	$targetFile = $uploadDir . $_FILES['Filedata']['name'];

	// Validate the filetype
	$fileParts = pathinfo($_FILES['Filedata']['name']);
	if (in_array(strtolower($fileParts['extension']), $fileTypes)) {

		// Save the file
		$fileName = time() . "." . strtolower($fileParts['extension']);
		$fileNameStatic = $fileName;
		$targetFile = $uploadDir . $fileNameStatic;
		move_uploaded_file($tempFile, $targetFile);
		$db->query("UPDATE auntie_customer_tbl SET profilePicUrl = '$fileNameStatic' WHERE customerID = '$customerID'");
		echo $fileNameStatic;

	} else {

		// The file type wasn't allowed
		echo 'Invalid file type.';

	}
}
?>